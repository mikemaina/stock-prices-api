﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innova.StockPricesImporter.Data.Model
{
    public class Stocks
    {
        public Guid StockID { get; set; }
        public Guid StockTypeId { get; set; }
        public Guid StockExchangeID { get; set; }
        public string Name { get; set; }
        public string ISIN { get; set; }
        public string Code { get; set; }
        public ICollection<StockPrices> StockPrices { get; set; }
    }
}
